# terraform-ansible-HetznerCloud-deploy-rancher

Automate the provisioning of infrastructure resources in Hetzner-Cloud by using Terraform and the deployment of Rancher on those resources with Ansible.


## How to use

## Requirements

WARNING: TEST/UBUNTU 18.04 in local machine

The following commands run on your ubuntu 18.04 local machine

apt update

apt install git

apt install unzip

ssh-keygen

Then...
 
1) Install Terraform 0.12

wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip

unzip terraform_0.12.24_linux_amd64.zip

mv terraform /usr/local/bin

terraform version


2) Install Ansible 2.9

sudo apt install software-properties-common

sudo apt-add-repository ppa:ansible/ansible

sudo apt update

sudo apt install ansible

ansible --version

### Clone this repository

```

git clone https://gitlab.com/daicarjim/rancher-in-docker-test-mode-in-hetznercloud.git
```


### Hetzner Cloud Token

To provision the infrastructure where Rancher (test-mode) will be installed, it is necessary to have an account in Hetzner Cloud. Once we have the account, we must give Terraform access so that it can create the infrastructure for us.

For this it is necessary to generate a token, go to the page: https://colinwilson.uk/2020/10/31/generate-an-api-token-in-hetzner-cloud/ this is a short-tutorial and create it.


### Create FILE with name terraform.tfvars(variables file)

1) enter the main project folder

cd rancher-in-docker-test-mode-in-hetznercloud

2) with command:

vim terraform.tfvars

3) Copy and paste this EXAMPLE in your file terraform.tfvars and CHANGE VARIABLES what your needed:


**within file terraform.tfvars**

hcloud_token = "COPY-YOUR-TOKEN-HETZNERCLOUD"

ssh_key_private = "~/.ssh/id_rsa"

ssh_public_key = "~/.ssh/id_rsa.pub"

server_name = "Rancher"

server_size = "cpx31"

server_region = "nbg1"



To deploy Rancher we just have to execute the following commands:
```

terraform init
terraform plan
terraform apply (write "yes" when terraform asks you)
```

Once the playbooks are finished, we can access our Rancher(test-mode) in the address IP.


GRATITUDE


https://github.com/galvarado/terraform-ansible-DO-deploy-wordpress



