variable "hcloud_token" {}
variable "ssh_key_private" {}
variable "ssh_public_key" {}
variable "server_name" {}
variable "server_size" {}
variable "server_region" {}

# Configure the Hetzner Provider
provider "hcloud" {
    token = var.hcloud_token
}

resource "hcloud_ssh_key" "k8s_admin" {
  name       = "k8s_admin"
  public_key = file(var.ssh_public_key)
}



# Create a web server
resource "hcloud_server" "myblog" {
    image         = "ubuntu-18.04"
    name          = var.server_name
    location      = var.server_region
    server_type   = var.server_size
    ssh_keys      = [hcloud_ssh_key.k8s_admin.id]

    # Install python on the droplet using remote-exec to execute ansible playbooks to configure the services
    provisioner "remote-exec" {
        inline = [
          "apt update -y","apt install software-properties-common -y","add-apt-repository ppa:deadsnakes/ppa -y","apt update -y","apt install python3.8 -y",
        ]

         connection {
            host        = self.ipv4_address
            type        = "ssh"
            user        = "root"
            private_key = file(var.ssh_key_private)
        }
    }

    # Execute ansible playbooks using local-exec 
    provisioner "local-exec" {
        environment = {
            PUBLIC_IP                 = "self.ipv4_address"
            PRIVATE_IP                = "self.ipv4_address_private"
            ANSIBLE_HOST_KEY_CHECKING = "False" 
        }
        

        working_dir = "docker_ubuntu1804/"
        command     = "ansible-playbook -u root --private-key ${var.ssh_key_private} -i ${self.ipv4_address}, playbook.yml"
    
    }

    provisioner "remote-exec" {
        inline = [
          "docker run -d --restart=unless-stopped -p 80:80 -p 443:443 -v /host/rancher:/var/lib/rancher --privileged rancher/rancher:latest",
        ]

         connection {
            host        = self.ipv4_address
            type        = "ssh"
            user        = "root"
            private_key = file(var.ssh_key_private)
        }
    }


}
